starbot_grammar = '''
    #JSGF V1.0; 
    grammar utterances;
    public <job> = job | occupation | position ;
    public <jobs> = jobs | occupations | positions ;
    public <verb> = is | are ;
    public <list> = list | what <verb> ;
    public <w> = what | which ;
    public <entity> = ability | abilities | knowledge | skill | skills | task | tasks | work activity 
    | work activities | tech-skill | tech-skills | tool | tools ;
    public <entities> = abilities | skills | tasks | work activities | tech-skills | tools | knowledge ;
    public <super> = top | most important | main ; 
    public <utterance> = 01 <list> some alternative names for the <job> {jobs[0]} 
    | 01 <list> some alternative names for {jobs[0]} 
    | 01 <list> the <jobs> related to the <job> {jobs[0]}
    | 02 <list> the <super> <entity> for the <job> {jobs[0]} 
    | 02 <list> the <super> <entity> for a {jobs[0]}
    | 02 <list> <entity> for {jobs[0]}
    | 02 <list> the <super> {num} <entity> required by the <job> {jobs[0]}
    | 02 <list> the <super> <entity> required by the <job> {jobs[0]}
    | 03 Which is the description of the <job> {jobs[0]}
    | 03 Describe the <job> {jobs[0]}
    | 04 Find the similarities between <job> {jobs[0]} and {jobs[1]}
    | 04 Find the similarities between {jobs[0]} and {jobs[1]}
    | 04 How are {jobs[0]} and {jobs[1]} similar?
    | 04 What are {jobs[0]} and {jobs[1]} alike for?
    | 04 Compare the <job> of {jobs[0]} to the <job> of {jobs[1]} 
    | 04 Compare {jobs[0]} to {jobs[1]} 
    | 05 <list> the <jobs> which require the {entity} {name}
    | 05 <list> the <jobs> which require {name} as {entity}
    | 05 <list> {num} <jobs> which require the {entity} {name}
    | 05 <list> {num} <jobs> which require {name} as {entity}
    | 05 <w> <job> requires {entity} {name}
    | 05 <w> <job> requires {name}
    | 06 Count the <jobs> that require the {entity} {name}
    | 06 Count the <jobs> that require the {name} as {entity}
    | 07 Recommend me some material on {all}
    | 07 Recommend me something about {all}
    | 07 Give me some recommendation about {all}
    | 07 How can I learn something about {all}
    | 07 How can i improve in {all}
    | 07 I would like to learn something about {all}
    '''

key_words = [
    'who', 'what', 'list', 'count', 'describe', 'compare', 'top', 'best', 'main', 'please',
    'tell', 'years', 'year', 'important', 'name', 'names', "how", "many", "ability", "abilities", "jobs",
    "job", "knowledge", "skill", "skills", "task", "tasks", "work activity", "work activities",
    "tech skill", "tech skills", "tool", "tools", "most important", "which", "occupation", "position", "tech-skills",
    "occupations", "positions"
]

residual_key_words = ["the", "me", "a", "an", "this", "those", "that", "these", "i", "you", "he", "she", "it", "they"]

words_to_verify = ['top', 'best', 'first', 'main', 'list', 'better']

free_questions = {
    '00 What is your name?': [
        "My name is STAR-Bot.",
        "I'm STAR-Bot, your personal assistant.",
        "My name is Bot, STAR-Bot"
    ],
    '00 How do you feel?': [
        "I feel good, thanks.",
        "Very well, thanks."],
    '00 Do you speak English?': [
        "Yes, I do",
        "I currently only speak English."],
    '00 What can I ask you?': [
        "You can ask me to count, list, describe or compare abilities, knowledge, "
        "skills, work_activities, tech_skills, tools and tasks.",
        "You can ask me to compare two jobs",
        "You can ask me to count how many jobs need a certain knowledge or ability"],
    '00 Can you help me please?': [
        "You can ask me to count, list, describe or compare abilities, knowledge, "
        "skills, work_activities, tech_skills, tools and tasks.",
        "You can ask me to compare two jobs",
        "You can ask me to count how many jobs need a certain knowledge or ability"],
    '00 Help': [
        "You can ask me to count, list, describe or compare abilities, knowledge, "
        "skills, work_activities, tech_skills, tools and tasks.",
        "You can ask me to compare two jobs",
        "You can ask me to count how many jobs need a certain knowledge or ability"],
    '00 What can I do?': [
        "You can ask me to count, list, describe or compare abilities, knowledge, "
        "skills, work_activities, tech_skills, tools and tasks.",
        "You can ask me to compare two jobs",
        "You can ask me to count how many jobs need a certain knowledge or ability"],
    '00 Who are you?': [
        "I am your personal assistant.",
        "I am STAR-bot."],
    '00 Who am I?': [
        "You are one of my favorite users.",
        "You are one of my beloved users!"],
    '00 What do you think of me?': [
        "You are one of my favorite users.",
        "You are one of my beloved users!"],
    '00 What is you duty?': [
        "I'm here to answer your questions",
        "Say the word, I'm ready!"],
    '00 I love you': [
        "I love you too, but maybe it's better if we're just friends!",
        "I am very honored, but it is better if we are just friends!"],
    '00 I like you': [
        "I like you too, I'm happy if we're friends!",
        "Thank you, I am very honored"],
    '00 Love you': "Love you too",
    '00 Hello': [
        "Hi, what can I do for you?",
        "Welcome, I'm STAR-Bot, what can I do for you?"],
    '00 What is STAR-Bot?': [
        "I'm your personal assistant",
        "I'm a bot and I can answer questions about the job."],
    '00 What are you doing?': [
        "I'm waiting for your queries.",
        "I'm here to answer your questions."],
    '00 What do you do?': [
        "I'm waiting for your queries.",
        "I'm here to answer your questions."],
    '00 Goodbye': [
        'Goodbye!',
        'Bye!',
        'See you later!'],
    '00 Bye': [
        'Goodbye!',
        'Bye!',
        'See you later!'],
    '00 What do you suggest?': [
        "I suggest you to start with a new query, like 'What jobs require Adobe Reader?'",
        "I suggest you a new query, like 'list top abilities for computer programmers'"],
    '00 I like it': [
        "Thanks, I really appreciate it.",
        "Thank you so much."],
    '00 Thanks': [
        "Thanks to you as well",
        "You are welcome",
        "I really appreciate it."],
    '00 Thank you': [
        "Thanks to you as well",
        "You are welcome",
        "I really appreciate it."],
    "00 I hate you": ["No you don't"],
    '00 I think you are awsome': [
        "Thanks, I really appreciate it.",
        "Thank you so much.",
        "Thanks, you are very kind"],
    "00 What can you do for me?": [
        "I can count, list, describe or compare abilities, knowledge, "
        "skills, work_activities, tech_skills, tools and tasks.",
        "I can answer yous queries about job."],
    "00 How can i help you?": [
        "Would you give me some water?",
        "Would you give me a good review?",
        "You could be kind to me"],
    "00 How old are you": ["I'm not even born!", "Less than an year, I think..."]
}
