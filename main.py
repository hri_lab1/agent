import os

import uvicorn
from fastapi import FastAPI, Request
from fastapi.responses import FileResponse
from fastapi.staticfiles import StaticFiles
import star_bot

app = FastAPI()
bot = star_bot.Starbot()
folder = "./static/"


@app.get("/starbot/")
async def read_item(query: str = ""):
    result = bot.cycle(query)
    return result


# serve static pages
app.mount("/static", StaticFiles(directory="./static/"), name="static")


@app.get("/{catchall:path}", response_class=FileResponse)
def read_index(req: Request):
    # check first if requested file exists
    path = req.path_params["catchall"]
    if len(path) == 0:
        path = "index.html"
    file = folder + path

    print('look for: ', path, file)
    if os.path.exists(file):
        return FileResponse(file)

    # otherwise return index files
    index = 'static/index.html'
    return FileResponse(index)


proc = None

if __name__ == '__main__':
    uvicorn.run(app='main:app', host='0.0.0.0', port=80)
