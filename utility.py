import csv
import json
import torch
from skills_query import QueryMysql
from sentence_transformers import SentenceTransformer

model = SentenceTransformer('all-MiniLM-L6-v2', device='cuda' if torch.cuda.is_available() else "cpu")


def save_st_job_titles(db_q):
    job_titles = db_q.get_job_titles()
    job_emb_titles = {}
    titles = [x[1] for x in job_titles]
    codes = [x[0] for x in job_titles]
    emb_titles = model.encode(titles)
    job_emb_titles["titles"] = titles
    job_emb_titles["codes"] = codes
    job_emb_titles["emb_titles"] = emb_titles
    torch.save(job_emb_titles, 'titles_embeddings.pt')


def save_skill_embeddings(db_q):
    data = db_q.get_skills_for_suggestion()
    for key in data:
        entities = data.get(key)
        names = [x[0] for x in entities]
        descr = [x[1] for x in entities]
        emb_names = model.encode(names)
        emb_descr = model.encode(descr)
        data[key] = {"names": names, "descr": descr, "emb_names": emb_names, "emb_descr": emb_descr}
    torch.save(data, 'bot_fixed_entities_embeddings.pt')


def save_task_embeddings(db_q):
    a = db_q.get_all_tasks()
    b = [x[0] for x in a]
    c = model.encode(b)
    data = {"tasks": b, "embeddings": c}
    torch.save(data, 'bot_task_embeddings.pt')


def save_tools_embeddings(db_q):
    a = db_q.get_all_tools()
    b = [x[0] for x in a]
    c = model.encode(b)
    data = {"tools": b, "embeddings": c}
    torch.save(data, 'bot_tools_embeddings.pt')


def save_tech_skill_embeddings(db_q):
    a = db_q.get_all_tech_skills()
    b = [x[0] for x in a]
    c = model.encode(b)
    data = {"tech_skills": b, "embeddings": c}
    torch.save(data, 'bot_tech_skills_embeddings.pt')


def valid_jobs(qdata):
    jobs = qdata.get_job_codes()
    ok_jobs = []
    ko_jobs = []
    vko_jobs = []
    tko_jobs = []
    for job in jobs:
        res = qdata.get_all_skills(job[0])
        a = len(res['abilities'])
        k = len(res['knowledge'])
        s = len(res['skills'])
        w = len(res['work_activities'])
        v_data = a + k + s + w
        ta = len(res['tasks'])
        te = len(res['tech_skills'])
        to = len(res['tools_used'])
        t_data =  ta + te + to

        if v_data < 161 and t_data == 0:
            ko_jobs.append([job[0], job[1], a, k, s, w, ta, te, to])
            print([job[0], job[1], a, k, s, w, ta, te, to])
        elif v_data < 161 and t_data > 0:
            vko_jobs.append([job[0], job[1], a, k, s, w, ta, te, to])
            print([job[0], job[1], a, k, s, w, ta, te, to])
        elif v_data == 161 and t_data == 0:
            tko_jobs.append([job[0], job[1], a, k, s, w, ta, te, to])
            print([job[0], job[1], a, k, s, w, ta, te, to])
        else:
            ok_jobs.append([job[0], job[1], a, k, s, w, ta, te, to])

    results = {"ok": ok_jobs, "v_ko": vko_jobs, "t_ko": tko_jobs, "ko": ko_jobs}

    try:
        with open("bot_job_data.json", "w", encoding="utf-8") as text_file:
            print(json.dumps(results), file=text_file)
    except IOError as e:
        print(e)

    header = ["onet_code", "job", "abilities", "knowledge", "skills", "work activities", "tasks", "tech-skills", "tools used"]
    f = open('job_data.csv', 'w', newline='')
    csv_writer = csv.writer(f, delimiter="\t")
    csv_writer.writerow(header)
    for row in ok_jobs:
        csv_writer.writerow(row)
    for row in vko_jobs:
        csv_writer.writerow(row)
    for row in tko_jobs:
        csv_writer.writerow(row)
    for row in ko_jobs:
        csv_writer.writerow(row)
    f.close()
    pass


if __name__ == "__main__":
    q = QueryMysql()
    save_skill_embeddings(q)
    save_st_job_titles(q)
    save_task_embeddings(q)
    save_tools_embeddings(q)
    save_tech_skill_embeddings(q)
    valid_jobs(q)
    pass
