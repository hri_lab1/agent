import json
import urllib.parse
import requests
import torch
from sentence_transformers.util import cos_sim
from models import model

star_courses = torch.load("bot_star_courses.pt")


def load_json(file__name):
    try:
        data_file = open(file__name, "r", encoding='utf-8')
        file = json.loads(data_file.read())
        data_file.close()
        return file
    except IOError:
        return {}


def udemy_request(query):
    courses = load_json("saved_queries.json")
    course = courses.get(query)
    if course is not None:
        return course

    header = {
        "Accept": "application/json, text/plain, */*",
        "Authorization": "your authorisation",
        "Content-Type": "application/json;charset=utf-8"
    }
    safe_string = urllib.parse.quote_plus(query)

    url = "https://www.udemy.com/api-2.0/courses?language=en&search="
    page_size = "&page_size=5"
    course_base_url = "https://www.udemy.com"
    try:
        r = requests.get(url + safe_string + page_size, headers=header)
        udemy = r.json().get("results")

        if udemy is None or len(udemy) == 0:
            course = {"result": "ko"}
        else:
            course_titles = [x.get("title") for x in udemy]
            best_course = get_similar(query, course_titles)
            
            if best_course[0] < 0.43:
                course = {"result": "ko"}
            else:
                udemy = udemy[course_titles.index(best_course[1])]
                course = {"result": "ok",
                          "title": udemy.get("title"),
                          "url": course_base_url + udemy.get("url"),
                          "image": udemy.get("image_125_H")}

        courses[query] = course
        with open('saved_queries.json', "w", encoding="utf-8") as text_file:
            print(json.dumps(courses), file=text_file)
        return course
    except requests.exceptions.RequestException as e:
        print(e)
        return {"result": "ko"}


def get_similarity(a, b):
    emb_a = model.encode(a)
    emb_b = model.encode(b)
    st_scores = cos_sim(emb_a, emb_b)
    maximus = torch.max(st_scores, 1)
    m = [float(x) for x in maximus.values]
    return m[0]


def get_similar(query, texts):
    embeddings = model.encode(texts)
    emb_question = model.encode(query)
    scores = cos_sim(emb_question, embeddings)
    maximus = torch.max(scores, 1)
    m = float(maximus.values[0])
    i = int(maximus.indices[0])
    return [round(m, 4), texts[i]]


def recommend(query):
    embeddings = star_courses.get("emb_names")
    courses = star_courses.get("courses")
    emb_question = model.encode(query)
    scores = cos_sim(emb_question, embeddings)
    maximus = torch.max(scores, 1)
    m = float(maximus.values[0])
    i = int(maximus.indices[0])
    star_course = courses[i]

    best_course = {
        "result": "ok",
        "title": star_course.get("Name"),
        "url": star_course.get("Website related"),
        "source": "STAR",
        "org": star_course.get("Website description"),
        "image": ""
    }

    if m >= 0.43:
        return best_course

    best_course = udemy_request(query)
    best_course["source"] = "Udemy"
    best_course["org"] = "Udemy"

    return best_course